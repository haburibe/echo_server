# JSON echo server

## Server setup

```
$ mkdir ./lib
$ pip install -t lib -r requirements.txt
$ dev_appserver.py . # or python server.py
```

## Client request

Using [httpie](https://github.com/jkbrzt/httpie)

```
$ http --json :8080/echo message="Hello, world"
HTTP/1.1 200 OK
Cache-Control: no-cache
Content-Length: 281
Date: Mon, 25 Apr 2016 16:12:57 GMT
Expires: Fri, 01 Jan 1990 00:00:00 GMT
Server: Development/2.0
content-type: application/json

{
    "headers": {
        "Accept": "application/json",
        "Content-Length": "27",
        "Content-Type": "application/json",
        "Host": "localhost:8080",
        "User-Agent": "HTTPie/0.9.3",
        "X-Appengine-Country": "ZZ"
    },
    "request_body": {
        "message": "Hello, world"
    }
}
```
