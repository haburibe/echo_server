# coding: utf-8
try:
    # appengine environment
    from google.appengine.ext import vendor
    vendor.add('lib')
except ImportError:
    # local environment
    import os.path
    import sys
    sys.path.append(
        os.path.join(os.path.dirname(os.path.abspath(__name__)), 'lib')
    )

from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/echo', methods=['POST'])
def echo():
    return jsonify(
        headers={field: value for (field, value) in request.headers},
        request_body=request.json
    )


if __name__ == '__main__':
    app.run(debug=True)
